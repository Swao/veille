<?php include "includes/global/db-connection.php" ?> 
<?php include "includes/global/veille.php" ?> 

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Article</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap"rel="stylesheet">
    <link rel="stylesheet" href="assets/css/styles.css">
    <link rel="stylesheet" href="assets/css/veille.css">
    <link rel="stylesheet" href="assets/css/navabar.css">
</head>

<body style="background-color: rgb(133, 163, 191);">

<?php include "includes/templates/nav.php" ?> 

<?php include "includes/script/articleVeille.php" ?> 

            
</body>

<script src="assets/js/navbar.js"></script>
<script src="assets/js/scroll.js"></script>
<script src="assets/js/openveille.js"></script>
<script>
    window.onscroll = function() {scrollFunction()};
    function scrollFunction() {
        var element = document.getElementById("test");
        if (document.body.scrollTop > 700 || document.documentElement.scrollTop > 700)
        {
            element.classList.add("scroll");
        } else {
            element.classList.remove("scroll");
        }
    }
</script>

</html>