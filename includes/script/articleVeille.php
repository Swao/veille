    <!-- <button class="btn btnColor mt-3" style="margin-left: 1rem;">
        <a style="color: white;" href="newFormulaire.html" target="_blank">+ Ajouter</a>
    </button> -->
    <div class="container" style="background-color: #FFFFFF;">
        <div class="text-center pt-2"> 
            <h1>VEILLE</h1>
        </div>  
        <h4>Derniers articles:</h4>
        <div class="row">
                <div class="col-12 col-md-6 col-lg-4 mt-2">  
                    <div class="cardBox" style="background: url('<?php echo $addVeille[0]['picture']; ?>') center / cover;">
                        <h3 class="rounded mt-5 ml-5 mr-5 text-center text-white btnColor" style="color: rgba(250, 250, 250, 1)"><?php echo $addVeille[0]['date']; ?></h3> 
                        <div class="card-footer">
                            <div>
                                <?php echo utf8_encode($addVeille[0]['subject']); ?>
                            </div>
                            <button class="btn btnColor mt-3" onclick="openVeille(<?php echo $addVeille[0]['id']; ?>)" target="_blank">En savoir plus</button>
                        </div>
                    </div>
                </div>     
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="cardBoxMid" style="background: url('<?php echo $addVeille[1]['picture']; ?>') center / cover; margin-top: 31px;">
                        <h3 class="rounded mt-5 ml-5 mr-5 text-center text-white btnColor" style="color: rgba(250, 250, 250, 1)"><?php echo $addVeille[1]['date']; ?></h3> 
                        <div class="cardFooterMid">
                            <div>
                                <?php echo utf8_encode($addVeille[1]['subject']); ?>
                            </div>
                            <button class="btn btnColor mt-3" onclick="openVeille(<?php echo $addVeille[1]['id']; ?>)" target="_blank">En savoir plus</button>
                        </div>
                    </div>
                    <div class="cardBoxMid" style="background: url('<?php echo $addVeille[2]['picture']; ?>') center / cover;">
                        <h3 class="rounded mt-5 ml-5 mr-5 text-center text-white btnColor" style="color: rgba(250, 250, 250, 1)">
                            <?php echo $addVeille[2]['date']; ?>
                        </h3> 
                        <div class="cardFooterMid">
                            <div>
                                <?php echo utf8_encode($addVeille[2]['subject']); ?>
                            </div>
                            <button class="btn btnColor mt-3" onclick="openVeille(<?php echo $addVeille[2]['id']; ?>)" target="_blank">En savoir plus</button>
                        </div>
                    </div>
                </div>
            <div class="col-12 col-md-4 col-lg-4">
                <h4>Toutes les Veilles</h4>
                <hr style="background-color: red;">
                <div class="row">
                    <div id="test">
                        <div id="addAllArticle" class="nolink scroll">
                            <?php for ($i=3; $i < sizeof($addVeille); $i++) { 
                                ?>
                                <div class="scrollBox" style="background: url('<?php echo $addVeille[$i]['picture']; ?>') center / cover;">
                                    <h3 class="rounded mt-5 ml-5 mr-5 text-center text-white btnColor" style="color: rgba(250, 250, 250, 1)"><?php echo $addVeille[$i]['date']; ?></h3> 
                                    <div class="subject">
                                        <?php echo utf8_encode($addVeille[$i]['subject']); ?>
                                    </div>
                                    <button class="btn btnColor mt-3" onclick="openVeille(<?php echo $addVeille[$i]['id']; ?>)" target="_blank">En savoir plus</button>
                                </div>
                                <?php
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


