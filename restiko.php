<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Restiko</title>
		<link rel="preconnect" href="https://fonts.gstatic.com">
		<link href="https://fonts.googleapis.com/css2?family=Allerta+Stencil&display=swap" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Nunito:400,700|Fugaz+One" rel="stylesheet">
		<link rel="preconnect" href="https://fonts.gstatic.com">
		<link href="https://fonts.googleapis.com/css2?family=Allerta+Stencil&family=Amatic+SC:wght@700&display=swap" rel="stylesheet">
		<link rel="stylesheet" href="boxicons\css\boxicons.css">
		<link rel="stylesheet" href="boxicons\fonts\boxicons.svg">
		<link rel="stylesheet" href="css/accordion.css">
		<link rel="stylesheet" href="css/restiko.css">
		<link rel="stylesheet" href="css/bootstrap-grid.css">
		<link rel="stylesheet" href="css/navabar.css">
		<link rel="stylesheet" href="css/wave.css">

		<script>document.documentElement.className = 'js';</script>
		
	</head>
	<body class="demo-3 loading">

	<?php include "navbar.php" ?>	

		<section>
			<div class="contentTitle">
			  <h2>Restiko</h2>
			</div>
			<svg class="wave" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
				<path fill="#fff" fill-opacity="1" d="M0,224L40,202.7C80,181,160,139,240,144C320,149,400,203,480,240C560,277,640,299,720,288C800,277,880,235,960,186.7C1040,139,1120,85,1200,64C1280,43,1360,53,1400,58.7L1440,64L1440,320L1400,320C1360,320,1280,320,1200,320C1120,320,1040,320,960,320C880,320,800,320,720,320C640,320,560,320,480,320C400,320,320,320,240,320C160,320,80,320,40,320L0,320Z"></path>
			</svg>
		</section>
		<button class="btn mt-3" style="margin-left: 1rem; background-color: #87A7F7;"><a style="color: white;" href="newFormulaireRestiko.html" target="_blank">+ Ajouter</a></button>
	
	<div >
		<div id="restiko" class="row">
			<?php include "includes/script/restiko.php"; ?>
		</div>
	</div>
	
			
			<script src="js/accordion.js"></script>
			<script src="js/restiko.js"></script>
			<script src="js/navbar.js"></script>
			<script >postRestiko()</script>
			<script src="js/date.picker.js"></script>
		</main>
	</body>
</html>
